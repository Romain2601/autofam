BEGIN TRANSACTION;
CREATE TABLE IF NOT EXISTS "camping_car" (
	"id"	INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE,
	"nom"	TEXT,
	"model"	TEXT,
	"type"	TEXT,
	"taille"	TEXT,
	"poids"	INTEGER,
	"nbPlaces"	INTEGER,
	"marque"	TEXT,
	"classe"	TEXT,
	"id_comments"	INTEGER
);
CREATE TABLE IF NOT EXISTS "comments" (
	"id"	INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE,
	"user"	TEXT,
	"commentaire"	INTEGER,
	"id_comments"	INTEGER,
	FOREIGN KEY("id_comments") REFERENCES "camping_car"
);
COMMIT;
