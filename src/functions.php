<?php

class CampingCar extends Model
{
    public static $_table = 'camping_car';

    public function comments()
    {
        return $this->has_many('Comment', 'id-camping')->find_many();
    }
}

class Comment extends Model{
    public static $_table = 'comments';
}