<?php
require "vendor/autoload.php";

Flight::map('render', function($template, $data=array()){
    Flight::view()->display($template, $data);
});

$loader = new Twig_Loader_Filesystem(dirname(__FILE__) . '/views');
$twigConfig = array(
    // 'cache' => './cache/twig/',
    // 'cache' => false,
    'debug' => true,
);

Flight::register('view', 'Twig_Environment', array($loader, $twigConfig), function ($twig) {
    $twig->addExtension(new Twig_Extension_Debug()); // Add the debug extension
});

Flight::before('start', function(&$params, &$output){
    ORM::configure('sqlite:data.sqlite3');
});

Flight::route('/', function(){
    $data = [
        'campingcars' => Model::factory("CampingCar")->find_many(),
    ];
    Flight::render('ind.twig', $data);
});

Flight::route('/presentation', function(){
    Flight::render('presentation.twig');
});


Flight::route('/camping-car/@nom', function($nom){
    $data = [
        'campingcar' => Model::factory("CampingCar")->where('nom', $nom)->find_one(),
    ];

    Flight::render('family.twig', $data);
});

Flight::route('/administration(/@id)', function($id){

    if($id){
        $campingcar = Model::factory('CampingCar')->where('id', $id)->find_one($id);
    }else{
        $campingcar = Model::factory('CampingCar')->create();
        $campingcar->nom = null;
        $campingcar->model = null;
        $campingcar->id = null;
        $campingcar->type = null;
        $campingcar->taille = null;
        $campingcar->poids = null;
        $campingcar->nbPlaces = null;
        $campingcar->chassis = null;
        $campingcar->classe = null;
        $campingcar->description = null;

    }
 
    

    if(Flight::request()->method == 'POST'){
        $campingcar->nom = Flight::request()->data->nom;
        $campingcar->model = Flight::request()->data->model;
        $campingcar->id = Flight::request()->data->id;
        $campingcar->type = Flight::request()->data->type;
        $campingcar->taille = Flight::request()->data->taille;
        $campingcar->poids = Flight::request()->data->poids;
        $campingcar->nbPlaces = Flight::request()->data->nbPlaces;
        $campingcar->chassis = Flight::request()->data->chassis;
        $campingcar->classe = Flight::request()->data->classe;
        $campingcar->description = Flight::request()->data->description;

        $campingcar->save();
    }

    Flight::render('administration.twig');
});

/**Ne  fonctionne pas*/
Flight::route('/camping-car/delete/@id', function($id){
    $campingcar = Model::factory('CampingCar')->find_one($id);
    Flash::success(sprintf('Camping-car %s supprimé!', $campingcar->nom));
    $campingcar->delete();
    Flight::redirect('/');
});

Flight::start();
?>